const problem4 = require("./problem4");
module.exports = function problem5(inventory) {
    const years = problem4(inventory);
    const arr = years.filter(car => car<2000);
    return arr;
}
