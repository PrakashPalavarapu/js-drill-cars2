module.exports = function problem3(inventory) {
    const arr = inventory.map(car => car["car_model"]);
    return arr.sort();
}
